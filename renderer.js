// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const fs = require('fs');
const rawConfig = fs.readFileSync('config.json');
const config = JSON.parse(rawConfig);

const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const port = new SerialPort(config['porta_serial'], {
    baudRate: 115200,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    highWaterMark: 32,
    hupcl: false
});

const strTitle = 'Carimbadeira ';

document.getElementById('description_title').textContent = `${strTitle} ${config['recurso']}`;

document.getElementById('portacom').textContent = config['porta_serial']
document.getElementById('recurso').textContent = config['recurso']
parser = port.pipe(new Readline({delimiter: '-', encoding:'utf8'}));

parser.on('data', function(data){
  console.log('parser data:', data);
})

let readedString = "";
let timeoutSerial = undefined;
port.on('data', dadosRecebidos =>{
  //console.log(new Date());
  readedString += dadosRecebidos.toString();

  if (timeoutSerial === undefined) {
    timeoutSerial = setTimeout(() => {
      const recurso = readedString.substr(0,3);
      let produto = readedString.substr(3);
      produto = produto.replace(/(\r\n|\n|\r)/gm,"").trim();

      if (produto.substr(-1) == 'U') {
        produto = produto.slice(0, -1);
      }

      if (produto.length < 3) {
        return;
      }

      timeoutSerial = undefined;
      readedString = "";

      //Chama a função de apontamento no production-form.js
      window.apontar(produto, recurso);
    },500);
  }
});

port.on('error', (err) => {
  document.getElementById('status_com').textContent = 'Erro de Comunicação';
  document.getElementById('status_com').style.color = 'Red';
  console.log('port err', err);
});

port.on('close', () => {
  document.getElementById('status_com').textContent = 'Porta Fechada';
  document.getElementById('status_com').style.color = 'Red';
  console.log('port close');
})

port.on('end', () => {
  document.getElementById('status_com').textContent = 'Porta Fechada';
  document.getElementById('status_com').style.color = 'Red';
  console.log('port end');
})

port.on('open', function(){
  document.getElementById('status_com').textContent = 'OK';
  document.getElementById('status_com').style.color = 'Green';
  console.log('port already open');
})

window.writeOnPort = (msg) => {
  port.write(msg + '\r');
};

