
const recursoConfigurado = config['recurso'];
const filename = recursoConfigurado;
const nosql = require('./node_modules/nosql');

const filenameDb = filename.toString();
const db = nosql.load(filenameDb);

const axios = require('axios');
const electron = require('electron');
const dialog = electron.remote.dialog;

/*const defaultHost = 'localhost';
const defaultPort = '/portal_hipper';
const defaultPrePath = '/';
const productionApi = 'scripts/php/ApontamentoCarimbadeiraOperacoes.php';*/

const defaultHost = '192.168.1.8';
const defaultPort = '80';
const defaultPrePath = '/protheus/';
const productionApi = 'scripts/php/ApontamentoCarimbadeiraOperacoes.php';

const http = require('http');

var jQuery = require('jQuery');
require('./vendor/select2/select2.min.js')(jQuery);

const panelMaxHeight = document.body.clientHeight - 492;
document.getElementById('production-list-container').style.height = panelMaxHeight;

const url = `http://${defaultHost}:${defaultPort}${defaultPrePath}${productionApi}?oper=gravaApontamento`;

let addDataToLastPrints = (apontamentos) => {
  document.getElementById('last_prints_tbody').innerHTML = '';
  let lastPrintsTbody = '';
  apontamentos.forEach((obj) => {
    const objData = JSON.stringify(obj);
    lastPrintsTbody += '<tr>';
    lastPrintsTbody += `<td>${obj['product']}</td>`;
    lastPrintsTbody += `<td>${obj['resource']}</td>`;
    lastPrintsTbody += `<td>${obj['quantity']}</td>`;
    lastPrintsTbody += `<td>${obj['date']}</td>`;
    lastPrintsTbody += `<td>${obj['hour']}</td>`;
    lastPrintsTbody += `<td>${obj['turn']}</td>`;
    lastPrintsTbody += '</tr>';
  });
  document.getElementById('last_prints_tbody').innerHTML = lastPrintsTbody;
}

//Grava o apontamento no banco local para caso não gravar no Protheus, gravar posteriormente
let gravaLocal = (productionData) => {

  let idCount = 0;

  db.scalar('max', ['id']).make(function(builder) {
    builder.callback(function(err, maxId) {
      if (!maxId){maxId = 0;}
      idCount = maxId + 1;
      gravaLocalDados();
    });
  });

  let gravaLocalDados = () => {
    db.insert({
      id: idCount,
      product: productionData['product'],
      resource: productionData['resource'],
      quantity: 1,
      date: productionData['date'],
      hour: productionData['time'],
      turn: productionData['turn']
    }).callback(function(err) {
      console.log('Gravado no banco local;');
    })
  };
}

//Busca ultimos apontamentos
let refreshLastPrints = () => {
  const urlbusca = `http://${defaultHost}:${defaultPort}${defaultPrePath}${productionApi}?oper=buscaUltimosApontamentos&filial=11&recurso=${recursoConfigurado}`;

  axios.get(urlbusca).then(function (response) {
    let formatoBr = ''
    const data = response.data;
    const apontamentos = data.apontamentos.map((apontamento) => {

      //Formatando data que vem do banco como 20201201 para 01/12/2020
      let formatoUS = apontamento['data'];
      let [ano, mes, dia] = formatoUS.match(/(\d{4})(\d{2})(\d{2})/).slice(1);
      formatoBr = [dia, mes, ano].join('/');
      return {
        product: apontamento['produto'],
        resource: apontamento['recurso'],
        quantity: apontamento['quantidade'],
        date: formatoBr,
        hour: apontamento['hora'],
        turn: apontamento['turno']
      };
    });
    addDataToLastPrints(apontamentos);
  });
};

refreshLastPrints();

const formataData = (dataAtual) => {
  const ano = dataAtual.getFullYear().toString();
  let mes = (dataAtual.getMonth() + 1).toString();
  let dia = dataAtual.getDate().toString();
  (dia.length == 1) && (dia =  `0${dia}`);
  (mes.length == 1) && (mes =  `0${mes}`);

  return `${ano}${mes}${dia}`;
};

//Grava dados que estavam no Banco Local por não terem sido gravados no Protheus
const gravaDadosOffline = () =>  {

  db.find().make(function(filter) {
    filter.where('name', '<>', 'Peter');
    filter.callback(function(err, response) {
      //console.log(response)
      const objDataOff = JSON.stringify(response);
      const offLineData = JSON.parse(objDataOff);

      db.count().make(function(builder) {
        builder.callback(function(err, count) {

          for (let i = 0; i < count; i++) {

            const offLineObject = {
              id: offLineData[i].id,
              branch: '11',
              resource: offLineData[i].resource,
              product: offLineData[i].product,
              status : 'P',
              quantity: 1,
              date: offLineData[i].date,
              time: offLineData[i].hour,
              turn: offLineData[i].turn
            };

            const idRemove = offLineObject.id;

            let formDataOffline = new URLSearchParams();
            for (let key in offLineObject ) {
              formDataOffline.append(key, offLineObject[key]);
            }
            axios.post(url, formDataOffline).then(function (response) {
              let dataResponseOffline = response.data;
              if (dataResponseOffline.valid) { //se gravou corretamente na tabela do Protheus
                refreshLastPrints();
                db.remove().make(function(builder) {
                  builder.where('id', '=', idRemove);
                  builder.callback(function(err, count) {
                      //console.log('Removido id: ', idRemove);
                  });
                });
              } else {
                console.log('Erro resposta valida gravar offline');
              }
            }).catch(function (error) {
              console.log('Erro post - gravar offline');
            })
          }
        });
      });
    });
  });
};


window.apontar = (produto, recurso) => {

  if (produto.substr(0, 2) != 'HF') {
    dialog.showMessageBox({ type: 'error', message: `Produto deve começar com HF! Produto informado: (${produto}) Recurso: (${recurso})`});
    return;
  }

  if (recurso < '300' || recurso > '399') {
    dialog.showMessageBox({ type: 'error', message: `Recurso deve ser configurado entre 300 e 399! Produto informado: (${produto}) Recurso: (${recurso})`});
    return;
  }

  let dataAtual = new Date();
  const horaAtual = dataAtual.getHours();
  const minutosAtual = dataAtual.getMinutes();
  const horaMinutoAtual = (horaAtual.toString().length == 1 ? '0' : '' ) + horaAtual.toString() + ':' + (minutosAtual.toString().length == 1 ? '0' : '' ) + minutosAtual.toString();
  if (horaAtual < 5) {
    dataAtual.setDate(dataAtual.getDate() - 1);
  }
  const dataFormatada = formataData(dataAtual);
  let turno = '';
  if (horaMinutoAtual < '05') {
      turno = '003';
  } else if (horaMinutoAtual <= '13:20'){
      turno = '001';
  } else if (horaMinutoAtual <= '21:40'){
      turno = '002';
  } else {
      turno = '003';
  };

  const productionObject = {
    branch: '11',
    resource: recurso,
    product: produto,
    status : 'P',
    quantity: 1,
    date: dataFormatada,
    time: horaMinutoAtual,
    turn: turno
  };
    const objData = JSON.stringify(productionObject);
    const productionData = JSON.parse(objData);

    let formData = new URLSearchParams();
    for (let key in productionData ) {
        formData.append(key, productionData[key]);
    }

    axios.post(url, formData).then(function (response) {
      let dataResponse = response.data;

      //se gravou corretamente na tabela do Protheus
      if (dataResponse.valid) {

        refreshLastPrints();
        gravaDadosOffline();
        window.writeOnPort('OK ON');
      } else {
        //gravaLocal(productionData);
        //window.writeOnPort('OK OFF');
        console.log("Erro ao gravar!");
        dialog.showMessageBox({ type: 'error', message: `Erro ao gravar! Produto informado: (${produto}) Recurso: (${recurso}) Detalhes: (${dataResponse.msg})`});
      }
    }).catch(function (error) {
      gravaLocal(productionData);
      console.log(error);
    })
}

