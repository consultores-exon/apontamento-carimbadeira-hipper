Apontamento de produção com carimbadeira Hipper Freios
======================

Instalação
---------------------
```
npm install
```

Desenvolvimento
---------------------
```
npm start
```

Distribuição
---------------------
```
electron-packager . --overwrite
```

Após Distribuição
---------------------
```
Criar uma pasta chamada node_modules na pasta do executável, copiar a pasta chamada "nosql"
e colar dentro da pasta node_modules criada
"apontamento-carimbadeira-hipper\node_modules\nosql"
```